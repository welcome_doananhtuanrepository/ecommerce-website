let cartIcon=document.querySelector("#cart-icon")
let cart=document.querySelector(".cart")
let closeCart=document.querySelector("#close-cart")
var cartContent=document.querySelector(".cart-content")
cartIcon.onclick=()=>{
    cart.classList.add("active")
}

closeCart.onclick=()=>{
    cart.classList.remove("active")
}

const handleRemove=(event)=>{
    event.target.parentElement.remove()
    updatetotal()
}

const handlePrice=(event,price)=>{
    if(event.target.value<=0){
        event.target.value=1
    }else{
        event.target.parentElement.querySelector(".cart-price").innerHTML=price*event.target.value
        updatetotal()
    }
}

// var quantityInputs=document.querySelectorAll(".cart-quantity")
// for(let i=0;i<quantityInputs.length;i++){
//     quantityInputs[i].addEventListener("change",handlePrice)
// }

const handleAddCart=(event)=>{
    const ele=event.target.parentElement
    
    const title=ele.querySelector(".produc-title").innerHTML
    const price=ele.querySelector(".price").innerHTML
    const img=ele.querySelector(".product-img").src
    console.log(title)

    var cartShopBox=document.createElement("div")
    var cartBox= cartContent.querySelectorAll(".cart-box")
    
    for(var i=0;i<cartBox.length;i++){
        if(cartBox[i].querySelector(".cart-ptoduct-title").innerHTML===title){
            alert("This item already in your card")
            return
        }
    }
    content=`
    <div class="cart-box">
    <img src="${img}" alt="" class="cart-img">
    <div class="detail-box">
        <div class="cart-ptoduct-title">${title}</div>
        <span>$</span><span class="cart-price">${price}</span><br/>
        <input type="number" value="1" class="cart-quantity" onchange="handlePrice(event,${price*1})">
    </div>
    <i class="fa-solid fa-trash cart-remove" onclick="handleRemove(event)" style="cursor:pointer;"></i>
    </div>     
    `
    cartShopBox.innerHTML=content
    cartContent.append(cartShopBox)
    updatetotal()
    
}

const addCart=document.querySelectorAll(".addCart")
for(var i=0; i<addCart.length;i++){
    addCart[i].addEventListener("click",handleAddCart)
}


// var removeCartButton=document.querySelectorAll(".cart-remove")
// for(let i=0;i<removeCartButton.length;i++){
//     var button=removeCartButton[i]
//     button.addEventListener("click",handleRemove)
// }

const updatetotal=()=>{
    
    var cartBoxes=cartContent.querySelectorAll(".cart-box")
    var total=0
    for(var i=0; i<cartBoxes.length;i++){
        var price=cartBoxes[i].querySelector(".cart-price").innerHTML
        var quantity=cartBoxes[i].querySelector(".cart-quantity").value*1
        total+=(price*quantity)
    }
    document.querySelector(".total-price").innerHTML=`$${total}`
}

updatetotal()

const handleBuy=()=>{
    cartContent.innerHTML=""
    updatetotal()
    alert("Your cart is ordered")
}


